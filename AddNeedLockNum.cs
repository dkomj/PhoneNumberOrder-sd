using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class AddNeedLockNum : Form
    {
        public AddNeedLockNum()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == "")
            {
                MessageBox.Show("手机号码不能为空！");
                return;
            }
            lock (Global.needLockNumList)
            {
                string rule = this.comboBox1.SelectedText == null ? "" : this.comboBox1.SelectedText;
                string num = this.textBox1.Text;
                Global.needLockNumList.Add(new string[] { this.comboBox1.Text, this.textBox1.Text, "0"});
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}