﻿namespace PhoneNumberOrder
{
    partial class PhoneNumbersCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt6 = new System.Windows.Forms.TextBox();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txt9 = new System.Windows.Forms.TextBox();
            this.txt8 = new System.Windows.Forms.TextBox();
            this.txt7 = new System.Windows.Forms.TextBox();
            this.txt11 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(1, 0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(21, 21);
            this.txt1.TabIndex = 0;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(29, 0);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(21, 21);
            this.txt2.TabIndex = 1;
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(57, 0);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(21, 21);
            this.txt3.TabIndex = 2;
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(153, 0);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(21, 21);
            this.txt6.TabIndex = 5;
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(125, 0);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(21, 21);
            this.txt5.TabIndex = 4;
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(97, 0);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(21, 21);
            this.txt4.TabIndex = 3;
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(249, 0);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(21, 21);
            this.txt9.TabIndex = 8;
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(221, 0);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(21, 21);
            this.txt8.TabIndex = 7;
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(181, 0);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(21, 21);
            this.txt7.TabIndex = 6;
            // 
            // txt11
            // 
            this.txt11.Location = new System.Drawing.Point(305, 0);
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(21, 21);
            this.txt11.TabIndex = 10;
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(277, 0);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(21, 21);
            this.txt10.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "-";
            // 
            // PhoneNumbersCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Name = "PhoneNumbersCtrl";
            this.Size = new System.Drawing.Size(336, 53);
            this.Load += new System.EventHandler(this.PhoneNumbersCtrl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt6;
        private System.Windows.Forms.TextBox txt5;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.TextBox txt9;
        private System.Windows.Forms.TextBox txt8;
        private System.Windows.Forms.TextBox txt7;
        private System.Windows.Forms.TextBox txt11;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
