﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace PhoneNumberOrder
{
    internal class SerializeHelper
    {
        public static bool Serialize(string filePath,object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream=null;
            try
            {
                stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, obj);
            }
            finally
            {
                if(stream!=null)
                    stream.Close();
            }
            return true;
        }

        public static object Deserialize(string filePath)
        {
            object obj = null;
            IFormatter formatter = new BinaryFormatter();
            Stream stream=null;
            try
            {
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                obj = formatter.Deserialize(stream);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return obj;
        }
    }
}
