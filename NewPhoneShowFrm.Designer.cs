﻿namespace PhoneNumberOrder
{
    partial class NewPhoneShowFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewPhoneShowFrm));
            this.pl_Content = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pl_Content
            // 
            this.pl_Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl_Content.Location = new System.Drawing.Point(0, 0);
            this.pl_Content.Name = "pl_Content";
            this.pl_Content.Size = new System.Drawing.Size(632, 471);
            this.pl_Content.TabIndex = 2;
            this.pl_Content.SizeChanged += new System.EventHandler(this.pl_Content_SizeChanged);
            // 
            // NewPhoneShowFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 471);
            this.Controls.Add(this.pl_Content);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewPhoneShowFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "最新重要号码列表";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pl_Content;
    }
}