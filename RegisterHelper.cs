using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace PhoneNumberOrder
{
    public class RegisterHelper
    {

        /**/
        /// <summary>
        /// 写入注册表
        /// </summary>
        /// <param name="strName"></param>
        public static void SetRegEditData(string strName, string strValue)
        {
            try
            {
                RegistryKey hklm = Registry.LocalMachine;
                RegistryKey software = hklm.OpenSubKey("SOFTWARE", true);
                RegistryKey aimdir = software.CreateSubKey("MySoftware");
                aimdir.SetValue(strName, strValue);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        /**/
        /// <summary>
        /// 修改注册表项
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="strValue"></param>
        public static void ModifyRegEditData(string strName, string strValue)
        {
            try
            {
                RegistryKey hklm = Registry.LocalMachine;
                RegistryKey software = hklm.OpenSubKey("SOFTWARE\\MySoftware", true);
                software.SetValue(strName, strValue);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /**/
        /// <summary>
        /// 判断指定注册表项是否存在
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public static bool IsExist(string strName)
        {
            try
            {
                bool exit = false;
                string[] subkeyNames;
                RegistryKey hkml = Registry.LocalMachine;
                RegistryKey software = hkml.OpenSubKey("SOFTWARE", true);
                RegistryKey aimdir = software.OpenSubKey("MySoftware", true);
                subkeyNames = aimdir.GetValueNames();
                foreach (string keyName in subkeyNames)
                {
                    if (keyName == strName)
                    {
                        exit = true;
                        return exit;
                    }
                }
                return exit;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}