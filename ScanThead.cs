using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PhoneNumberOrder
{
    public class ScanThead
    {
        public ManualResetEvent set = null;
        public LockScanThreadObject lockObj = null;
        public bool isNeedHistory = false;
        public NumRuleObject ruleObject = null;
        public int threadId = -1;
        public string strMsg = "";
        public string strUseTime = "";

        public void Run()
        {
            MainFrm.self.scanThead(set, lockObj, isNeedHistory, ruleObject, 
                threadId, strMsg, strUseTime);
        }
    }

    public class LockScanThreadObject
    {
        private int index = -1;

        public int Add()
        {
            lock (this)
            {
                index++;
                return index;
            }
        }
    }
}
