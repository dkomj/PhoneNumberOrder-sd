using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace PhoneNumberOrder
{
    public class Encryper
    {
        private static RijndaelCryptography m_Instane;
        public static string Encrypto(string Source)
        {
            if (m_Instane == null)
                m_Instane = new RijndaelCryptography();
            return m_Instane.Encrypto(Source);
        }

        public static string Decrypto(string Source)
        {
            if (m_Instane == null)
                m_Instane = new RijndaelCryptography();
            return m_Instane.Decrypto(Source);
        }

    }

    /// <summary>
    /// 对称加密类
    /// </summary>
    public class RijndaelCryptography
    {
        RijndaelManaged myRijndael;
        ASCIIEncoding textConverter;
        byte[] fromEncrypt;
        byte[] encrypted;
        byte[] toEncrypt;
        byte[] _key;
        byte[] _IV;

        private byte[] Key
        {
            get { return _key; }
            set { _key = value; }
        }
        private byte[] IV
        {
            get { return _IV; }
            set { _IV = value; }
        }
        private byte[] Encrypted
        {
            get { return encrypted; }
        }

        public RijndaelCryptography()
        {
            myRijndael = new RijndaelManaged();
            myRijndael.Mode = CipherMode.CBC;
            textConverter = new ASCIIEncoding();
        }

        #region 公开方法
        /// <summary>
        /// 加密方法
        /// </summary>
        /// <param name="Source">待加密的串</param>
        /// <returns>经过加密的串</returns>
        public string Encrypto(string Source)
        {
            byte[] bytIn = UTF8Encoding.UTF8.GetBytes(Source);
            MemoryStream ms = new MemoryStream();
            myRijndael.Key = GetLegalKey();
            myRijndael.IV = GetLegalIV();
            ICryptoTransform encrypto = myRijndael.CreateEncryptor();
            CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);
            cs.Write(bytIn, 0, bytIn.Length);
            cs.FlushFinalBlock();
            ms.Close();
            byte[] bytOut = ms.ToArray();
            return Convert.ToBase64String(bytOut);
        }


        /// <summary>
        /// 解密方法
        /// </summary>
        /// <param name="Source">待解密的串</param>
        /// <returns>经过解密的串</returns>
        public string Decrypto(string Source)
        {
            byte[] bytIn = Convert.FromBase64String(Source);
            MemoryStream ms = new MemoryStream(bytIn, 0, bytIn.Length);
            myRijndael.Key = GetLegalKey();
            myRijndael.IV = GetLegalIV();
            ICryptoTransform encrypto = myRijndael.CreateDecryptor();
            CryptoStream cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Read);
            StreamReader sr = new StreamReader(cs);
            return sr.ReadToEnd();
        }
        #endregion

        protected virtual void GenKey()
        {
            //Create a new key and initialization vector.
            myRijndael.GenerateKey();
            myRijndael.GenerateIV();

            //Get the key and IV.
            _key = myRijndael.Key;
            _IV = myRijndael.IV;
        }

        private void Encrypt(string TxtToEncrypt)
        {
            //Get an encryptor.
            ICryptoTransform encryptor = myRijndael.CreateEncryptor(_key, _IV);

            //Encrypt the data.
            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //Convert the data to a byte array.
            toEncrypt = textConverter.GetBytes(TxtToEncrypt);

            //Write all data to the crypto stream and flush it.
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            encrypted = msEncrypt.ToArray();
        }

        private string Decrypt(byte[] crypted)
        {
            //Get a decryptor.
            ICryptoTransform decryptor = myRijndael.CreateDecryptor(_key, _IV);

            //Decrypting the encrypted byte array.
            MemoryStream msDecrypt = new MemoryStream(crypted);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            fromEncrypt = new byte[crypted.Length];

            //Read the data out of the crypto stream.
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

            //Convert the byte array into a string.
            return textConverter.GetString(fromEncrypt);


        }


        /// <summary>
        /// 获得密钥
        /// </summary>
        /// <returns>密钥</returns>
        protected virtual byte[] GetLegalKey()
        {
            //string sTemp = "Guz(%&hj7x89H$yuBI0456FtmaT5&fvHUFCy76*h%(HilJ$lhj!y6&(*jkP87jH7"; ;
            string sTemp = "Guz(%&hj7x89H$yuBI04omFtmaT5&fvHUFCy76*h%(HilJ$lhj!om&(*jkP87jH7"; ;
            myRijndael.GenerateKey();
            byte[] bytTemp = myRijndael.Key;
            int KeyLength = bytTemp.Length;
            if (sTemp.Length > KeyLength)
                sTemp = sTemp.Substring(0, KeyLength);
            else if (sTemp.Length < KeyLength)
                sTemp = sTemp.PadRight(KeyLength, ' ');
            return ASCIIEncoding.ASCII.GetBytes(sTemp);
        }


        /// <summary>
        /// 获得初始向量IV
        /// </summary>
        /// <returns>初试向量IV</returns>
        protected virtual byte[] GetLegalIV()
        {
            //string sTemp = "E4ghj*Ghg7!rNIfb&95GUY86GfghUb#er57HBh(u%g6HJ($jhWk7&!hg4ui%$hjk";
            string sTemp = "E4ghj*Ghg7!rNIfb&95GUY86OmghUb#er57HBh(u%g6HJ($jhWk7&!hgOmi%$hjk";
            myRijndael.GenerateIV();
            byte[] bytTemp = myRijndael.IV;
            int IVLength = bytTemp.Length;
            if (sTemp.Length > IVLength)
                sTemp = sTemp.Substring(0, IVLength);
            else if (sTemp.Length < IVLength)
                sTemp = sTemp.PadRight(IVLength, ' ');
            return ASCIIEncoding.ASCII.GetBytes(sTemp);
        }


    }
}
