using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace PhoneNumberOrder
{
    public partial class NewPhoneShowFrm : Form
    {
        public NewPhoneShowFrm()
        {
            InitializeComponent();
        }

        public void showFrm()
        {
            this.Show();
        }

        private void pl_Content_SizeChanged(object sender, EventArgs e)
        {
            showFrm();
        }

    }
}