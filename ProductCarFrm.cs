using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class ProductCarFrm : Form
    {
        private bool m_OnlyLast4 = false;
        private string m_Filter = "";
        public ProductCarFrm()
        {
            InitializeComponent();
        }

        private void ProductCarFrm_Load(object sender, EventArgs e)
        {
            button1_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            this.checkBox1.Checked = false;
            button3_Click(null, null);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            m_Filter = this.textBox1.Text;
            this.m_OnlyLast4 = this.checkBox1.Checked;
            int recordCount = this.getCount();
            this.pagerCtrl1.SetRecordCount(recordCount, true);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1_Click(null, null);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (this.dataGridView1.CurrentRow == null)
            //{
            //    MessageBox.Show("请先选择号码");
            //    return;
            //}

            //string[] item = (string[])this.dataGridView1.CurrentRow.Tag;
            //bool isNeedLockNum = true;
            //if (item[4]=="true")
            //{
            //    if (DateTime.Parse(item[2]).AddMinutes(30) > DateTime.Now)
            //        isNeedLockNum = false;
            //}

            //if(isNeedLockNum)
            //{
            //    string[] temp_car = MainFrm.self.lockNum(item[0], item[1]);
            //    if (temp_car != null && temp_car[2] == "true")
            //        MainFrm.self.showProductCar(temp_car[0], temp_car[1]);
            //    else
            //    {
            //        MessageBox.Show("号码锁定失败！请重试！");
            //    }
            //}
            //else
            //{
            //    MainFrm.self.showProductCar(item[3], item[5]);
                
            //}
        }

        private int getCount()
        {
            int iCount = 0;
            int index = -1;
            if (this.m_OnlyLast4)
                index += 7;


            int len = Global.lockNumList.Count;
            if (m_Filter != null)
            {
                for (int i = 0; i < len; i++)
                {
                    if (Global.lockNumList[i][1].IndexOf(m_Filter) > index)
                        iCount++;
                }
            }
            else
            {
                iCount = Global.lockNumList.Count;
            }

            return iCount;
        }

        private void pagerCtrl1_OnRefreshClick(object sender, EventArgs e)
        {
            int firstNum = this.pagerCtrl1.PageSzie * (this.pagerCtrl1.CurrPageIndex - 1) + 1;
            int lastNum = this.pagerCtrl1.PageSzie * (this.pagerCtrl1.CurrPageIndex);
            int num = 0;
            int index = -1;
            if (this.m_OnlyLast4)
                index += 7;

            this.dataGridView1.Rows.Clear();
            DataGridViewRow row;
            int len = Global.lockNumList.Count;
            for (int i = len - 1; i > -1; i--)
            {
                string[] item = Global.lockNumList[i];
                bool isOk = true;
                if (m_Filter != "" && item[1].IndexOf(m_Filter) <= index)
                    isOk = false;

                if (isOk)
                {
                    num++;
                    if (num >= firstNum && num <= lastNum)
                    {
                        this.dataGridView1.Rows.Add();

                        row = this.dataGridView1.Rows[this.dataGridView1.RowCount - 1];
                        row.Cells[0].Value = item[0];
                        row.Cells[1].Value = item[1];

                        if (item.Length > 6)
                        {
                            row.Cells[2].Value = item[6];
                            row.Cells[3].Value = item[7];
                        }
                        else
                        {
                            row.Cells[2].Value = "";
                            row.Cells[3].Value = "";
                        }
                        row.Cells[4].Value = item[2];
                        row.Cells[5].Value = item[4];
                        row.Tag = item;
                    }
                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要清空锁定时间超过半小时的号码吗？", "确认", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //MainFrm.self.clearTimeOutLockNum();
                this.button3_Click(null, null);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("号码规则");
            dt.Columns.Add("手机号码");
            dt.Columns.Add("预存");
            dt.Columns.Add("保底");
            dt.Columns.Add("锁定状态");

            int index = 0;
            if (this.m_OnlyLast4)
                index += 7;

            int len = Global.lockNumList.Count;
            for (int i = len - 1; i > -1; i--)
            {
                string[] item = Global.lockNumList[i];
                bool isOk = true;
                if (m_Filter != "" && item[1].IndexOf(m_Filter) <= index)
                    isOk = false;

                if (isOk)
                {
                    DataRow newRow = dt.NewRow();
                    newRow[0] = item[0];
                    newRow[1] = item[1];
                    newRow[4] = item[4];
                    if (item.Length > 6)
                    {
                        newRow[2] = item[6];
                        newRow[3] = item[7];
                    }
                    else
                    {
                        newRow[2] = "";
                        newRow[3] = "";
                    }

                    dt.Rows.Add(newRow);
                }
            }

            MainFrm.RenderToExcel(dt);
        }


    }
}