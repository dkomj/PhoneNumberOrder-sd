using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class PhoneNumbersCtrl : UserControl
    {
        public PhoneNumbersCtrl()
        {
            InitializeComponent();
        }

        public void SetReadOnly(bool isReadOnly)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).ReadOnly = isReadOnly;
                }
            }
        }

        public String GetFilterStr()
        {
            string r = "";
            string[] a = new string[] { "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*" };
            foreach (Control c in this.Controls)
            {
                if (c is TextBox && c.Text.Trim() != "")
                {
                    int index = int.Parse(c.Name.Replace("txt","")) - 1;
                    a[index] = c.Text.Trim();
                }
            }
            foreach (string s in a)
            {
                r += s;
            }

            if (r == "***********")
                r = "";

            return r;
        }

        private void PhoneNumbersCtrl_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).TextAlign = HorizontalAlignment.Center;
                    c.KeyPress += new KeyPressEventHandler(txt_KeyPress);
                }
            }
        }

        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            TextBox self = (TextBox)sender;
            TextBox txtNext;
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                self.Text = e.KeyChar.ToString();
                txtNext = findNumTxt(self.Name, 1);
                if (txtNext != null)
                {
                    txtNext.Focus();
                    txtNext.Select();
                }
            }
            if (e.KeyChar == '\b')
            {
                if (self.Text != "")
                {
                    self.Text = "";
                    return;
                }

                txtNext = findNumTxt(self.Name, -1);
                if (txtNext != null)
                {
                    txtNext.Focus();
                    txtNext.Select();
                }
            }
            else if (e.KeyChar == '\r' || e.KeyChar == ' ')
            {
                txtNext = findNumTxt(self.Name, 1);
                if (txtNext != null)
                {
                    txtNext.Focus();
                    txtNext.Select();
                }
            }
            //else
            //{
            //    self.Text = e.KeyChar.ToString();
            //    txtNext = findNumTxt(self.Name, 1);
            //    if (txtNext != null)
            //    {
            //        txtNext.Focus();
            //        txtNext.Select();
            //    }
            //}
        }

        private TextBox findNumTxt(string txtName,int dis)
        {
            int currIndex = -1; 
            TextBox ctrl = null;
            if (int.TryParse(txtName.Replace("txt", ""), out currIndex))
            {
                currIndex += dis;
                string strNewTxtId = "txt" + currIndex.ToString();
                foreach (Control c in this.Controls)
                {
                    if (c is TextBox && c.Name == strNewTxtId)
                    {
                        ctrl = (TextBox)c;
                        break;
                    }
                }
            }
            return ctrl;
        }
    }
}
