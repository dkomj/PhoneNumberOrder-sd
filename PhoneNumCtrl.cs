﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace PhoneNumberOrder
{
    public partial class PhoneNumCtrl : UserControl
    {
        private PhoneOrder m_Phone = null;
        private IList m_NewPhoneList = null;
        public PhoneNumCtrl()
        {
            InitializeComponent();
        }

        public void init(PhoneOrder phone,IList list)
        {
            m_Phone = phone;
            m_NewPhoneList = list;
            this.lbNum.Text = m_Phone.m_Num;
            this.label1.Text = string.Format("预存：￥{0}", m_Phone.m_PrestoreMoney);
            this.label2.Text = string.Format("保底：￥{0}/月", m_Phone.m_BaseFeeMoney);
            if(m_Phone.m_Order != null)
            {
                this.panel1.BackColor = System.Drawing.Color.Green;
            }
        }

        private void lb_Order_Click(object sender, EventArgs e)
        {
            if (this.m_Phone.m_Order == null)
            {
                OrderFrm frm = new OrderFrm();
                frm.m_Phone = this.m_Phone;
                frm.ShowDialog();
            }
            else
            {
                OrderSuccessFrm frm = new OrderSuccessFrm();
                frm.Text = "查看预约信息";
                frm.info = this.m_Phone.m_Order.ToString();
                frm.ShowDialog();
            }
           
        }

        private void lb_Remove_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("确定要移除该号码吗？", "确认", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                m_NewPhoneList.Remove(m_Phone);
                MainFrm.self.refreshNewPhone();
            }
        }
    }
}
