﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneNumberOrder
{
    public class NumRuleObject
    {
        public string num;
        public string name;
        public List<string[]> opts;

        public static NumRuleObject create(string anum,string aname,string param)
        {
            NumRuleObject o = new NumRuleObject();
            o.num = anum;
            o.name = aname;
            o.opts = new List<string[]>();
            if (!String.IsNullOrEmpty(param))
            {
                string[] ar1 = param.Split(new char[] { '#' });
                foreach (string str in ar1)
                {
                    string[] ar2 = str.Split(new char[] { '@' });
                    o.opts.Add(new string[] { ar2[1], ar2[0] });
                }
            }
            return o;
        }
    }
}
