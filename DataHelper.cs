using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using log4net;

namespace PhoneNumberOrder
{
    public class DataHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static object m_LockObj = new object();

        private static DataHelper m_Instance;
        public static DataHelper Instance
        {
            get
            {
                lock (m_LockObj)
                {
                    if (m_Instance == null)
                        m_Instance = new DataHelper();
                    return m_Instance;
                }
            }
        }
        private string curr = "";
        private int step = 0;

        private DataHelper()
        {
            lock (m_LockObj)
            {
                if (!MainFrm.IsDataOk)
                {
                    if (!System.IO.File.Exists(Global.tokenUserPath))
                    {
                        curr = DateTime.Now.ToString("yyyy-MM-dd");
                        step = 1;
                        List<string> user = new List<string>();
                        user.Add(Encryper.Encrypto(curr));
                        user.Add(Encryper.Encrypto(step.ToString()));
                        SerializeHelper.Serialize(Global.tokenUserPath, user);
                    }

                    try
                    {
                        RegistryKey location = Registry.LocalMachine;
                        RegistryKey soft = location.OpenSubKey("SOFTWARE", false);//可写 
                        RegistryKey dkomj = soft.OpenSubKey("DKOMJ_SD_NEW2", false);
                        this.curr = Encryper.Decrypto(dkomj.GetValue("curr").ToString());
                        this.step = int.Parse(Encryper.Decrypto(dkomj.GetValue("step").ToString()));
                    }
                    catch
                    {
                        try
                        {
                            RegistryKey location = Registry.LocalMachine;
                            RegistryKey soft = location.OpenSubKey("SOFTWARE", true);//可写
                            RegistryKey dkomj = soft.CreateSubKey("DKOMJ_SD_NEW2");
                            this.curr = DateTime.Now.ToString("yyyy-MM-dd");
                            this.step = 1;
                            dkomj.SetValue("curr", Encryper.Encrypto(curr));
                            dkomj.SetValue("step", Encryper.Encrypto(step.ToString()));
                        }
                        catch
                        {
                            log.Error("加载出错...");
                            List<string> user = (List<string>)SerializeHelper.Deserialize(Global.tokenUserPath);
                            curr = Encryper.Decrypto(user[0]);
                            step = int.Parse(Encryper.Decrypto(user[1]));

                        }
                    }
                }   
            }
        }

        private void AddCount()
        {
            lock (m_LockObj)
            {
                string time = DateTime.Now.ToString("yyyy-MM-dd");
                if (curr != time)
                {
                    int count = step + 1;
                    List<string> user = new List<string>();
                    user.Add(Encryper.Encrypto(time));
                    user.Add(Encryper.Encrypto(count.ToString()));
                    SerializeHelper.Serialize(Global.tokenUserPath, user);
                    curr = time;
                    step = count;

                    try
                    {
                        RegistryKey location = Registry.LocalMachine;
                        RegistryKey soft = location.OpenSubKey("SOFTWARE", true);//可写
                        RegistryKey dkomj = soft.OpenSubKey("DKOMJ_SD_NEW2", true);
                        dkomj.SetValue("curr", Encryper.Encrypto(curr));
                        dkomj.SetValue("step", Encryper.Encrypto(step.ToString()));
                    }
                    catch
                    {
                        log.Error("加载出错...");
                    }
                }
            }
        }

        public bool CheckValid(Object data)
        {
            if (!MainFrm.IsDataOk)
            {
                AddCount();
                if (step > 3)
                    throw new ApplicationException("非法数据！");
            }
            return true;
        }
    }
}
