﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class RegisterFrm : Form
    {
        public RegisterFrm()
        {
            InitializeComponent();
        }

        private void RegisterFrm_Load(object sender, EventArgs e)
        {
            this.textBox1.Text = MainFrm.Md5(Computer.Instance().GetSeriesNumber());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> data = new List<string>();
            data.Add(this.textBox2.Text);
            if (checkIsSoftDogOk(data))
            {
                SerializeHelper.Serialize(Global.softdogFilePath, data);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("注册码无效！");
            }
        }

        public static bool checkIsSoftDogOk(List<string> input)
        {
            try
            {
                string code = MainFrm.Md5(Computer.Instance().GetSeriesNumber());
                string inputStr = Encryper.Decrypto(input[0]);
                if (code == inputStr)
                    return true;

                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}