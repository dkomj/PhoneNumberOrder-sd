﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public class Global
    {
        public static string userListFilePath = Path.Combine(Application.StartupPath, "orderuser.data");
        public static List<string[]> userList = new List<string[]>();//预订帐号

        public static string orderSuccessListPath = Path.Combine(Application.StartupPath, "ordersuccess.data");
        public static List<OrderInfo> orderSuccessList = new List<OrderInfo>();//预订成功信息

        public static string lockNumFilePath = Path.Combine(Application.StartupPath, "lockphone.data");
        public static List<string[]> lockNumList = new List<string[]>();//锁定号码列表
        public static List<string[]> needLockNumList = new List<string[]>();//需要锁定的号码列表

        public static string tokenUserPath = Path.Combine(Application.StartupPath, "user.token");

        public static string softdogFilePath = Path.Combine(Application.StartupPath, "softdog.dll");
    }
}
