using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace PhoneNumberOrder
{
    public partial class OrderFrm : Form
    {
        private static int index = -1;

        public PhoneOrder m_Phone;


        //public string cityid = "402881ea3286d488013286d7e1da0006";
        //public string regionid
        //public string regionNm = "%D4%BD%B3%C7";

        private string m_SessonId = "";
        private Encoding m_Encoding = Encoding.GetEncoding("GB2312");
        public OrderFrm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OrderFrm_Load(object sender, EventArgs e)
        {
            if (Global.userList.Count > 0)
            {
                this.label4.Text = this.m_Phone.m_Num;
                int num = index % Global.userList.Count;
                this.getNextUser();
            }
            this.refreshImageCode();
            this.textBox4.Focus();
        }

        private void getNextUser()
        {
            if (Global.userList.Count > 0)
            {
                string[] user = null;
                for (int i = 0; i < Global.userList.Count; i++)
                {
                    if (int.Parse(Global.userList[i][3]) < 1)
                    {
                        user = Global.userList[i];
                        index = i;
                        continue;
                    }
                }

                if (user == null)
                {
                    index++;
                    int num = index % Global.userList.Count;
                    user = Global.userList[num];
                }

                this.textBox1.Text = user[0];
                this.textBox2.Text = user[1];
                this.textBox3.Text = user[2];
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string regionNm = "%D4%BD%B3%C7";
            string regionid = "8ac6008e33af761f0133b0a63af604f7";
            RadioButton rbtn = null;
            if (radioButton1.Checked)
                rbtn = radioButton1;
            else if (radioButton2.Checked)
                rbtn = radioButton2;
            else if (radioButton3.Checked)
            {
                rbtn = radioButton3;
                regionNm = "%D4%BD%B3%C7";
                regionid = "8ac6008e33af761f0133b0a90c6d0521";
            }
            else if (radioButton4.Checked)
                rbtn = radioButton4;

            //if (DateTime.Now.Ticks - 635188608080000600 > 0)
            //    return;

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                string data = string.Format("telCode={0}&cityid=402881ea3286d488013286d7e1da0006&regionNm={6}&radio={1}&numOfficeId=&isShare=Y&officeId={1}&regionid={7}&userName={5}&select1=%C9%ED%B7%DD%D6%A4&userCardNum={2}&cellPhone={3}&yzm={4}",
                        this.m_Phone.m_Num, rbtn.Tag, this.textBox2.Text, this.textBox3.Text, this.textBox4.Text, urlEncod(this.textBox1.Text), regionNm, regionid);

                 Uri uri = new Uri("http://www.zj.10086.cn/");
                socket.Connect(uri.Host, uri.Port);
                string strResult = "";
                string strSendInfo = "POST /shop/shop/contract/addContract.do HTTP/1.1\r\n"
                                    + "Content-Type: application/x-www-form-urlencoded\r\n"
                                    + "Content-Length: " + data.Length.ToString() + "\r\n"
                                    + "Connection: Keep-Alive\r\n"
                                    + "Host: www.zj.10086.cn\r\n"
                                    + "Cookie: " +m_SessonId+ "\r\n\r\n"
                                    + data + "\r\n";
                socket.Send(this.m_Encoding.GetBytes(strSendInfo));
                try
                {
                    strResult = this.getSocketReceiveText(socket, "</html>");
                    OrderSuccessFrm frm = new OrderSuccessFrm();
                    if(strResult.IndexOf("</html>") < 0)
                    {
                        frm.info = strResult;
                        frm.Text = "号码预约失败";
                        frm.ShowDialog();
                        return;
                    }

                    string pattern = "\\<label\\>([^<]+)\\<";
                    Regex reg = new Regex(pattern, RegexOptions.Multiline);
                    OrderInfo ord = new OrderInfo();

                    Match ma = reg.Match(strResult);
                    ord.m_OrderNum = ma.Groups[1].ToString();
                    ma = ma.NextMatch();
                    ord.m_PhoneNum = this.m_Phone.m_Num;//ma.Groups[1].ToString();
                    ma = ma.NextMatch();
                    ord.m_OrderPwd = ma.Groups[1].ToString();
                    ord.m_OfficeName = rbtn.Text;
                    ord.m_UserName = this.textBox1.Text;
                    ord.m_UserCardId = this.textBox2.Text;
                    ord.m_Tel = this.textBox3.Text;
                    frm.Text = "号码预约成功";
                    frm.info = ord.ToString();
                    frm.ShowDialog();

                    Global.orderSuccessList.Add(ord);
                    SerializeHelper.Serialize(Global.orderSuccessListPath,Global.orderSuccessList);
                    this.m_Phone.m_Order = ord;

                    if (Global.userList[index][1] == this.textBox2.Text)
                    {
                        Global.userList[index][3] = string.Format("{0}", int.Parse(Global.userList[index][3]) + 1);
                        SerializeHelper.Serialize(Global.userListFilePath, Global.userList);
                    }
                    MainFrm.self.refreshNewPhone();
                    this.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("预约出错！" + ex.Message);
                }
            }
            finally
            {
                socket.Close();
            }
        }

        private void refreshImageCode()
        {
            try
            {
                string fileName = Path.Combine(Application.StartupPath, "imgCode.jpg");
                string url = "http://www.zj.10086.cn/shop/sinoaptcha1.jpg?rand=" + Guid.NewGuid().ToString();
                WebClient client = new WebClient();
                client.DownloadFile(url, fileName);
                this.m_SessonId = client.ResponseHeaders["Set-Cookie"];
                this.pictureBox1.Load(fileName);
            }
            catch (System.Exception)
            {

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.refreshImageCode();
        }

        public string getSocketReceiveText(Socket socket, string strEndTag)
        {
            string strResult = "";
            StringBuilder sb = new StringBuilder();
            socket.ReceiveTimeout = 5 * 1000;//5秒阻塞
            byte[] receivedBytes = new byte[1024 * 100];
            try
            {
                int receiveCount = 0;
                int totelCount = 0;
                while (true)
                {
                    receiveCount = socket.Receive(receivedBytes, totelCount, receivedBytes.Length - totelCount, SocketFlags.None);
                    totelCount += receiveCount;
                    strResult = this.m_Encoding.GetString(receivedBytes);
                    if (strResult.Contains(strEndTag))
                    {
                        string strPattern = "\r\n[0-9a-f]{1,}\r\n";
                        Regex reg = new Regex(strPattern, RegexOptions.IgnoreCase);
                        strResult = reg.Replace(strResult, "");
                        break;
                    }

                    if (receiveCount == 0)
                        break;
                }
            }
            catch (Exception)
            {
                //throw new Exception("没能接收到服务器正常数据包！可能与服务器会话丢失，或者服务器不正常！");
            }
            finally
            {
                socket.Close();
            }

            return strResult;
        }

        private string urlEncod(string input)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = this.m_Encoding.GetBytes(input);
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }
            return sb.ToString().ToUpper();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.getNextUser();
        }
    }
}