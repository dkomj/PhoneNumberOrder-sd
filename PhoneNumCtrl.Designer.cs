﻿namespace PhoneNumberOrder
{
    partial class PhoneNumCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_Remove = new System.Windows.Forms.Label();
            this.lb_Order = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbNum = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lb_Remove);
            this.panel1.Controls.Add(this.lb_Order);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lbNum);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 79);
            this.panel1.TabIndex = 0;
            // 
            // lb_Remove
            // 
            this.lb_Remove.AutoSize = true;
            this.lb_Remove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_Remove.ForeColor = System.Drawing.Color.Blue;
            this.lb_Remove.Location = new System.Drawing.Point(116, 56);
            this.lb_Remove.Name = "lb_Remove";
            this.lb_Remove.Size = new System.Drawing.Size(29, 12);
            this.lb_Remove.TabIndex = 4;
            this.lb_Remove.Text = "移除";
            this.lb_Remove.Click += new System.EventHandler(this.lb_Remove_Click);
            // 
            // lb_Order
            // 
            this.lb_Order.AutoSize = true;
            this.lb_Order.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_Order.ForeColor = System.Drawing.Color.Blue;
            this.lb_Order.Location = new System.Drawing.Point(116, 35);
            this.lb_Order.Name = "lb_Order";
            this.lb_Order.Size = new System.Drawing.Size(29, 12);
            this.lb_Order.TabIndex = 3;
            this.lb_Order.Text = "预约";
            this.lb_Order.Click += new System.EventHandler(this.lb_Order_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "保底：￥5000/月";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "预存：￥5000";
            // 
            // lbNum
            // 
            this.lbNum.AutoSize = true;
            this.lbNum.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold);
            this.lbNum.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbNum.Location = new System.Drawing.Point(7, 4);
            this.lbNum.Name = "lbNum";
            this.lbNum.Size = new System.Drawing.Size(142, 22);
            this.lbNum.TabIndex = 0;
            this.lbNum.Text = "12345678901";
            // 
            // PhoneNum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel1);
            this.Name = "PhoneNum";
            this.Size = new System.Drawing.Size(155, 79);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lb_Order;
        private System.Windows.Forms.Label lb_Remove;
    }
}
