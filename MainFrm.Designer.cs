﻿namespace PhoneNumberOrder
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.lb1 = new System.Windows.Forms.Label();
            this.cbk1 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbVerifyCodeNum = new System.Windows.Forms.Label();
            this.btnSaveVCode = new System.Windows.Forms.Button();
            this.txtVerifyCode = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.chkNoFour = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.phoneNumbersCtrl1 = new PhoneNumberOrder.PhoneNumbersCtrl();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lb7 = new System.Windows.Forms.Label();
            this.lb6 = new System.Windows.Forms.Label();
            this.lb_Zt = new System.Windows.Forms.Label();
            this.lb5 = new System.Windows.Forms.Label();
            this.lb4 = new System.Windows.Forms.Label();
            this.lb3 = new System.Windows.Forms.Label();
            this.lb2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Scan = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbk7 = new System.Windows.Forms.CheckBox();
            this.cbk6 = new System.Windows.Forms.CheckBox();
            this.cbk5 = new System.Windows.Forms.CheckBox();
            this.cbk4 = new System.Windows.Forms.CheckBox();
            this.cbk3 = new System.Windows.Forms.CheckBox();
            this.cbk2 = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pl_Content = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pl_Content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton5,
            this.toolStripButton4});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::PhoneNumberOrder.Properties.Resources.user;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(97, 22);
            this.toolStripButton1.Text = "预约帐号维护";
            this.toolStripButton1.Visible = false;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::PhoneNumberOrder.Properties.Resources.Win;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(97, 22);
            this.toolStripButton2.Text = "预约信息查询";
            this.toolStripButton2.Visible = false;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::PhoneNumberOrder.Properties.Resources.Win;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(121, 22);
            this.toolStripButton3.Text = "扫描历史号码查询";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::PhoneNumberOrder.Properties.Resources.Win;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(97, 22);
            this.toolStripButton5.Text = "锁定号码查询";
            this.toolStripButton5.Visible = false;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::PhoneNumberOrder.Properties.Resources.Win;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(109, 22);
            this.toolStripButton4.Text = "需锁定号码维护";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(1008, 248);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.comboBox3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.lb1);
            this.groupBox1.Controls.Add(this.cbk1);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbVerifyCodeNum);
            this.groupBox1.Controls.Add(this.btnSaveVCode);
            this.groupBox1.Controls.Add(this.txtVerifyCode);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.chkNoFour);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.phoneNumbersCtrl1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lb7);
            this.groupBox1.Controls.Add(this.lb6);
            this.groupBox1.Controls.Add(this.lb_Zt);
            this.groupBox1.Controls.Add(this.lb5);
            this.groupBox1.Controls.Add(this.lb4);
            this.groupBox1.Controls.Add(this.lb3);
            this.groupBox1.Controls.Add(this.lb2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_Scan);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbk7);
            this.groupBox1.Controls.Add(this.cbk6);
            this.groupBox1.Controls.Add(this.cbk5);
            this.groupBox1.Controls.Add(this.cbk4);
            this.groupBox1.Controls.Add(this.cbk3);
            this.groupBox1.Controls.Add(this.cbk2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(998, 238);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "扫描筛选条件设置";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(382, 167);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(49, 21);
            this.textBox3.TabIndex = 65;
            this.textBox3.Text = "50";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(318, 171);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 12);
            this.label14.TabIndex = 64;
            this.label14.Text = "扫描线程：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(526, 171);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 12);
            this.label13.TabIndex = 63;
            this.label13.Text = "label13";
            this.label13.Visible = false;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(142, 167);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(168, 20);
            this.comboBox3.TabIndex = 62;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 12);
            this.label2.TabIndex = 61;
            this.label2.Text = "潍坊极品靓扫描专区：";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(437, 166);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 60;
            this.button7.Text = "开始扫描";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.ForeColor = System.Drawing.Color.Red;
            this.lb1.Location = new System.Drawing.Point(412, 29);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(23, 12);
            this.lb1.TabIndex = 58;
            this.lb1.Text = "1、";
            // 
            // cbk1
            // 
            this.cbk1.AutoSize = true;
            this.cbk1.Checked = true;
            this.cbk1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk1.Location = new System.Drawing.Point(391, 29);
            this.cbk1.Name = "cbk1";
            this.cbk1.Size = new System.Drawing.Size(132, 16);
            this.cbk1.TabIndex = 59;
            this.cbk1.Text = "启用线程[极品靓号]";
            this.cbk1.UseVisualStyleBackColor = true;
            this.cbk1.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(730, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(233, 12);
            this.label12.TabIndex = 57;
            this.label12.Text = "注：录入验证码，可以在输入框按回车保存";
            this.label12.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(730, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(209, 12);
            this.label9.TabIndex = 56;
            this.label9.Text = "注：本版本暂时不支持验证码自动识别";
            this.label9.Visible = false;
            // 
            // lbVerifyCodeNum
            // 
            this.lbVerifyCodeNum.AutoSize = true;
            this.lbVerifyCodeNum.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbVerifyCodeNum.ForeColor = System.Drawing.Color.Red;
            this.lbVerifyCodeNum.Location = new System.Drawing.Point(410, 202);
            this.lbVerifyCodeNum.Name = "lbVerifyCodeNum";
            this.lbVerifyCodeNum.Size = new System.Drawing.Size(242, 21);
            this.lbVerifyCodeNum.TabIndex = 55;
            this.lbVerifyCodeNum.Text = "剩余可用验证码（0）个";
            // 
            // btnSaveVCode
            // 
            this.btnSaveVCode.Location = new System.Drawing.Point(299, 201);
            this.btnSaveVCode.Name = "btnSaveVCode";
            this.btnSaveVCode.Size = new System.Drawing.Size(83, 23);
            this.btnSaveVCode.TabIndex = 54;
            this.btnSaveVCode.Text = "保存验证码";
            this.btnSaveVCode.UseVisualStyleBackColor = true;
            this.btnSaveVCode.Click += new System.EventHandler(this.btnSaveVCode_Click);
            // 
            // txtVerifyCode
            // 
            this.txtVerifyCode.Location = new System.Drawing.Point(189, 202);
            this.txtVerifyCode.Name = "txtVerifyCode";
            this.txtVerifyCode.Size = new System.Drawing.Size(106, 21);
            this.txtVerifyCode.TabIndex = 53;
            this.txtVerifyCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVerifyCode_KeyPress);
            this.txtVerifyCode.Enter += new System.EventHandler(this.txtVerifyCode_Enter);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(135, 206);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(41, 12);
            this.linkLabel1.TabIndex = 51;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "换一张";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(22, 194);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(781, 209);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(158, 21);
            this.dateTimePicker1.TabIndex = 49;
            this.dateTimePicker1.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(841, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 48;
            this.label11.Text = "扫描定时停止：";
            this.label11.Visible = false;
            // 
            // chkNoFour
            // 
            this.chkNoFour.AutoSize = true;
            this.chkNoFour.Location = new System.Drawing.Point(116, 91);
            this.chkNoFour.Name = "chkNoFour";
            this.chkNoFour.Size = new System.Drawing.Size(90, 16);
            this.chkNoFour.TabIndex = 47;
            this.chkNoFour.Text = "不包含4号码";
            this.chkNoFour.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 12);
            this.label10.TabIndex = 46;
            this.label10.Text = "扫描号码过滤：";
            // 
            // phoneNumbersCtrl1
            // 
            this.phoneNumbersCtrl1.Location = new System.Drawing.Point(22, 115);
            this.phoneNumbersCtrl1.Name = "phoneNumbersCtrl1";
            this.phoneNumbersCtrl1.Size = new System.Drawing.Size(336, 22);
            this.phoneNumbersCtrl1.TabIndex = 45;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(180, 41);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 21);
            this.textBox1.TabIndex = 39;
            this.textBox1.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(164, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 38;
            this.label8.Text = "-";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Hangzhou,杭州",
            "Ningbo",
            "Wenzhou",
            "Shaoxing",
            "Jiaxing",
            "Zhoushan",
            "Jinhua",
            "Quzhou",
            "Taizhou",
            "Lishui",
            "Huzhou"});
            this.comboBox2.Location = new System.Drawing.Point(116, 17);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(106, 20);
            this.comboBox2.TabIndex = 29;
            this.comboBox2.Text = "Shaoxing";
            this.comboBox2.TextChanged += new System.EventHandler(this.comboBox2_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(717, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "测试警报声量";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(590, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "关闭本次警报声";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(116, 65);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(106, 21);
            this.textBox4.TabIndex = 24;
            this.textBox4.Text = "2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 23;
            this.label7.Text = "扫描线程数量：";
            // 
            // lb7
            // 
            this.lb7.AutoSize = true;
            this.lb7.ForeColor = System.Drawing.Color.Red;
            this.lb7.Location = new System.Drawing.Point(411, 132);
            this.lb7.Name = "lb7";
            this.lb7.Size = new System.Drawing.Size(23, 12);
            this.lb7.TabIndex = 22;
            this.lb7.Text = "7、";
            // 
            // lb6
            // 
            this.lb6.AutoSize = true;
            this.lb6.ForeColor = System.Drawing.Color.Red;
            this.lb6.Location = new System.Drawing.Point(411, 115);
            this.lb6.Name = "lb6";
            this.lb6.Size = new System.Drawing.Size(23, 12);
            this.lb6.TabIndex = 21;
            this.lb6.Text = "6、";
            // 
            // lb_Zt
            // 
            this.lb_Zt.AutoSize = true;
            this.lb_Zt.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Zt.ForeColor = System.Drawing.Color.Red;
            this.lb_Zt.Location = new System.Drawing.Point(398, 16);
            this.lb_Zt.Name = "lb_Zt";
            this.lb_Zt.Size = new System.Drawing.Size(122, 12);
            this.lb_Zt.TabIndex = 20;
            this.lb_Zt.Text = "系统当前状态：空闲";
            // 
            // lb5
            // 
            this.lb5.AutoSize = true;
            this.lb5.ForeColor = System.Drawing.Color.Red;
            this.lb5.Location = new System.Drawing.Point(411, 98);
            this.lb5.Name = "lb5";
            this.lb5.Size = new System.Drawing.Size(23, 12);
            this.lb5.TabIndex = 19;
            this.lb5.Text = "5、";
            // 
            // lb4
            // 
            this.lb4.AutoSize = true;
            this.lb4.ForeColor = System.Drawing.Color.Red;
            this.lb4.Location = new System.Drawing.Point(411, 81);
            this.lb4.Name = "lb4";
            this.lb4.Size = new System.Drawing.Size(23, 12);
            this.lb4.TabIndex = 18;
            this.lb4.Text = "4、";
            // 
            // lb3
            // 
            this.lb3.AutoSize = true;
            this.lb3.ForeColor = System.Drawing.Color.Red;
            this.lb3.Location = new System.Drawing.Point(411, 64);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(23, 12);
            this.lb3.TabIndex = 17;
            this.lb3.Text = "3、";
            // 
            // lb2
            // 
            this.lb2.AutoSize = true;
            this.lb2.ForeColor = System.Drawing.Color.Red;
            this.lb2.Location = new System.Drawing.Point(411, 47);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(23, 12);
            this.lb2.TabIndex = 16;
            this.lb2.Text = "2、";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "地市：";
            // 
            // btn_Scan
            // 
            this.btn_Scan.Location = new System.Drawing.Point(241, 25);
            this.btn_Scan.Name = "btn_Scan";
            this.btn_Scan.Size = new System.Drawing.Size(132, 77);
            this.btn_Scan.TabIndex = 5;
            this.btn_Scan.Text = "开始扫描";
            this.btn_Scan.UseVisualStyleBackColor = true;
            this.btn_Scan.Click += new System.EventHandler(this.btn_Scan_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(116, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(42, 21);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "预存金额范围：";
            // 
            // cbk7
            // 
            this.cbk7.AutoSize = true;
            this.cbk7.Location = new System.Drawing.Point(390, 130);
            this.cbk7.Name = "cbk7";
            this.cbk7.Size = new System.Drawing.Size(132, 16);
            this.cbk7.TabIndex = 36;
            this.cbk7.Text = "启用线程[普通号码]";
            this.cbk7.UseVisualStyleBackColor = true;
            this.cbk7.CheckedChanged += new System.EventHandler(this.cbk8_CheckedChanged);
            // 
            // cbk6
            // 
            this.cbk6.AutoSize = true;
            this.cbk6.Checked = true;
            this.cbk6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk6.Location = new System.Drawing.Point(390, 113);
            this.cbk6.Name = "cbk6";
            this.cbk6.Size = new System.Drawing.Size(132, 16);
            this.cbk6.TabIndex = 35;
            this.cbk6.Text = "启用线程[星座号码]";
            this.cbk6.UseVisualStyleBackColor = true;
            this.cbk6.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // cbk5
            // 
            this.cbk5.AutoSize = true;
            this.cbk5.Checked = true;
            this.cbk5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk5.Location = new System.Drawing.Point(390, 96);
            this.cbk5.Name = "cbk5";
            this.cbk5.Size = new System.Drawing.Size(132, 16);
            this.cbk5.TabIndex = 34;
            this.cbk5.Text = "启用线程[吉祥号码]";
            this.cbk5.UseVisualStyleBackColor = true;
            this.cbk5.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // cbk4
            // 
            this.cbk4.AutoSize = true;
            this.cbk4.Checked = true;
            this.cbk4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk4.Location = new System.Drawing.Point(390, 79);
            this.cbk4.Name = "cbk4";
            this.cbk4.Size = new System.Drawing.Size(132, 16);
            this.cbk4.TabIndex = 33;
            this.cbk4.Text = "启用线程[爱情号码]";
            this.cbk4.UseVisualStyleBackColor = true;
            this.cbk4.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // cbk3
            // 
            this.cbk3.AutoSize = true;
            this.cbk3.Checked = true;
            this.cbk3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk3.Location = new System.Drawing.Point(390, 63);
            this.cbk3.Name = "cbk3";
            this.cbk3.Size = new System.Drawing.Size(132, 16);
            this.cbk3.TabIndex = 32;
            this.cbk3.Text = "启用线程[生日号码]";
            this.cbk3.UseVisualStyleBackColor = true;
            this.cbk3.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // cbk2
            // 
            this.cbk2.AutoSize = true;
            this.cbk2.Checked = true;
            this.cbk2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbk2.Location = new System.Drawing.Point(390, 47);
            this.cbk2.Name = "cbk2";
            this.cbk2.Size = new System.Drawing.Size(132, 16);
            this.cbk2.TabIndex = 31;
            this.cbk2.Text = "启用线程[规则号码]";
            this.cbk2.UseVisualStyleBackColor = true;
            this.cbk2.CheckedChanged += new System.EventHandler(this.cbk1_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 273);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel2.Size = new System.Drawing.Size(1008, 269);
            this.panel2.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pl_Content);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(5, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(998, 264);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "锁定号码列表";
            // 
            // pl_Content
            // 
            this.pl_Content.Controls.Add(this.dataGridView1);
            this.pl_Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl_Content.Location = new System.Drawing.Point(3, 57);
            this.pl_Content.Name = "pl_Content";
            this.pl_Content.Size = new System.Drawing.Size(992, 204);
            this.pl_Content.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Column5,
            this.Column6,
            this.Column4,
            this.Column2});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(992, 204);
            this.dataGridView1.TabIndex = 7;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "号码规则";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "手机号码";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "预存";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "低消";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "锁定时间";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "锁定状态";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.btn_Reset);
            this.panel3.Controls.Add(this.btn_Search);
            this.panel3.Controls.Add(this.txt_Search);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 17);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(992, 40);
            this.panel3.TabIndex = 0;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(890, 9);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(121, 23);
            this.button6.TabIndex = 27;
            this.button6.Text = "删除选中锁定号码";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(737, 9);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(72, 23);
            this.button4.TabIndex = 26;
            this.button4.Text = "导出Excel";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(812, 9);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(73, 23);
            this.button5.TabIndex = 25;
            this.button5.Text = "放入购物车";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "全部";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "全部",
            "AAAAA",
            "AAAA",
            "AAA",
            "ABAB",
            "AABB",
            "88",
            "Nomarl"});
            this.comboBox1.Location = new System.Drawing.Point(70, 10);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(79, 20);
            this.comboBox1.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 23;
            this.label6.Text = "号码规则：";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(780, 10);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "查看最新重要号码";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(485, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 21;
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(395, 9);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(75, 23);
            this.btn_Reset.TabIndex = 11;
            this.btn_Reset.Text = "重置";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(318, 9);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 10;
            this.btn_Search.Text = "查询";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(217, 10);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(92, 21);
            this.txt_Search.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "手机号码：";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 542);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainFrm";
            this.Text = "手机号码扫描工具";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFrm_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.pl_Content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn_Scan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lb5;
        private System.Windows.Forms.Label lb4;
        private System.Windows.Forms.Label lb3;
        private System.Windows.Forms.Label lb2;
        private System.Windows.Forms.Label lb_Zt;
        private System.Windows.Forms.Label lb6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pl_Content;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.CheckBox cbk6;
        private System.Windows.Forms.CheckBox cbk5;
        private System.Windows.Forms.CheckBox cbk4;
        private System.Windows.Forms.CheckBox cbk3;
        private System.Windows.Forms.CheckBox cbk2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private PhoneNumbersCtrl phoneNumbersCtrl1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkNoFour;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label lb7;
        private System.Windows.Forms.CheckBox cbk7;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtVerifyCode;
        private System.Windows.Forms.Button btnSaveVCode;
        private System.Windows.Forms.Label lbVerifyCodeNum;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Label lb1;
        private System.Windows.Forms.CheckBox cbk1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label14;
        

    }
}

