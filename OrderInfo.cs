﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneNumberOrder
{
    [Serializable]
    public class OrderInfo
    {
        public OrderInfo()
        {
            m_Time=DateTime.Now;
            m_OrderMethod="手动预约";
        }

        public string m_OfficeName="";
        public string m_PhoneNum = "";
        public string m_OrderNum = "";
        public string m_OrderPwd = "";
        public string m_UserName = "";
        public string m_UserCardId = "";
        public string m_Tel = "";
        public DateTime m_Time;
        public string m_OrderMethod = "";

        public override string ToString()
        {
            string msg = "预约单号:" + this.m_OrderNum;
            msg += "\r\n预约号码:" + this.m_PhoneNum;
            msg += "\r\n查询密码:" + this.m_OrderPwd;
            msg += "\r\n预约营业厅:" + this.m_OfficeName;
            msg += "\r\n客户姓名:" + this.m_UserName;
            msg += "\r\n身份证号:" + this.m_UserCardId;
            msg += "\r\n联系电话:" + this.m_Tel;
            return msg;
        }

        public string ToString2()
        {
            string msg = "预约单号:" + this.m_OrderNum;
            msg += ",预约号码:" + this.m_PhoneNum;
            msg += ",查询密码:" + this.m_OrderPwd;
            msg += ",预约营业厅:" + this.m_OfficeName;
            msg += ",客户姓名:" + this.m_UserName;
            msg += ",身份证号:" + this.m_UserCardId;
            msg += ",联系电话:" + this.m_Tel;
            return msg;
        }
    }
}
