using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class OrderSuccessFrm : Form
    {
        public string info;
        public OrderSuccessFrm()
        {
            InitializeComponent();
        }

        private void OrderSuccessFrm_Load(object sender, EventArgs e)
        {
            this.textBox1.Text = info;
        }
    }
}