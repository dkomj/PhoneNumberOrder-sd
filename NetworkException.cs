﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneNumberOrder
{
    class NetworkException : ApplicationException
    {
        public NetworkException(string msg,Exception ex) : base(msg,ex)
        {

        }
    }
}
