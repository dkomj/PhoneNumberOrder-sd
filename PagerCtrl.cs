using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class PagerCtrl : UserControl
    {
        private int m_PageSzie = 50;
        private int m_CurrPageIndex = 1;
        private int m_RecordCount = 0;//记录总数
        private int m_PageCount = 0; 

        #region 自定义 属性,事件
        //[Browsable(true)]
        //[Category("打开首页事件")]
        //public event EventHandler OnFirstClick;

        //[Browsable(true)]
        //[Category("打开上一页")]
        //public event EventHandler OnPreviousClick;

        //[Browsable(true)]
        //[Category("打开下一页")]
        //public event EventHandler OnNextClick;

        //[Browsable(true)]
        //[Category("打开最后页")]
        //public event EventHandler OnLastClick;

        [Browsable(true)]
        [Category("刷新事件")]
        public event EventHandler OnRefreshClick;

        public int PageSzie
        {
            get
            {
                return m_PageSzie;
            }
        }
        public int CurrPageIndex
        {
            get
            {
                return m_CurrPageIndex;
            }
        }

        private int RecordCount 
        {
            get
            {
                return m_RecordCount;
            }
        }
        #endregion

        public PagerCtrl()
        {
            InitializeComponent();
            this.SetRecordCount(0, true);
        }

        public void SetRecordCount(int recordCount, bool isSwitchFirst)
        {
            this.m_RecordCount = recordCount;
            if (isSwitchFirst)
                this.m_CurrPageIndex = 1;

            m_PageCount = this.m_RecordCount / this.m_PageSzie;
            if (this.m_RecordCount % this.m_PageSzie != 0)
                m_PageCount++;
            this.setButtonEnable();
        }

        private void setButtonEnable()
        {
            this.toolStripButton1.Enabled = false;
            this.toolStripButton2.Enabled = false;
            this.toolStripButton3.Enabled = false;
            this.toolStripButton4.Enabled = false;

            if(this.m_CurrPageIndex < this.m_PageCount)
            {
                this.toolStripButton3.Enabled = true;
                this.toolStripButton4.Enabled = true;
            }

            if(this.m_CurrPageIndex > 1)
            {
                this.toolStripButton1.Enabled = true;
                this.toolStripButton2.Enabled = true;
            }

            this.toolStripTextBox1.Text = this.m_CurrPageIndex.ToString();
            this.toolStripLabel1.Text = string.Format(" / {0}", this.m_PageCount);
            this.toolStripLabel2.Text = string.Format("   每页 {0} 条，共 {1} 条",this.m_PageSzie,this.m_RecordCount);
            refreshEvent();
        }

        private void refreshEvent()
        {
            if (OnRefreshClick != null)
                OnRefreshClick(this, null);
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.m_CurrPageIndex = 1;
            this.setButtonEnable();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.m_CurrPageIndex--;
            this.setButtonEnable();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.m_CurrPageIndex++;
            this.setButtonEnable();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            this.m_CurrPageIndex = this.m_PageCount;
            this.setButtonEnable();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.setButtonEnable();
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==13)
            {
                int index = 1;
                if (int.TryParse(this.toolStripTextBox1.Text, out index))
                {
                    if(index > m_PageCount)
                        index = m_PageCount;
                    if (index < 1)
                        index = 1;
                    this.m_CurrPageIndex = index;
                    this.SetRecordCount(this.m_PageCount, true);
                }
                else
                {
                    this.toolStripTextBox1.Text = this.m_CurrPageIndex.ToString();
                }
            }
        }

        private void toolStripComboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.ChangePageSize();
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ChangePageSize();
        }

        private void ChangePageSize()
        {
             int index = 1;
             bool bl = int.TryParse(this.toolStripComboBox1.Text, out index);
             if(bl && index > 0)
             {
                 this.m_PageSzie = index;
                 this.SetRecordCount(this.m_RecordCount, true);
             }
             else
             {
                 this.toolStripComboBox1.Text = this.m_PageSzie.ToString();
             }
        }
    }
}

