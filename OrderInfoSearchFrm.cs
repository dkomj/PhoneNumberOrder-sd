using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PhoneNumberOrder
{
    public partial class OrderInfoSearchFrm : Form
    {
        public OrderInfoSearchFrm()
        {
            InitializeComponent();
        }

        private string m_Filter = "";

        private void OrderInfoSearchFrm_Load(object sender, EventArgs e)
        {
            refreshDataGrid(true,true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要删除当前选中的预约信息记录吗？\r\n\r\n注意：此操作不可恢复。", "询问",
                MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
                {
                    Global.orderSuccessList.Remove((OrderInfo)row.Tag);
                }

                SerializeHelper.Serialize(Global.orderSuccessListPath, Global.orderSuccessList);
                refreshDataGrid(true,false);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.m_Filter = this.textBox1.Text;
            refreshDataGrid(true,true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.m_Filter = "";
            button1_Click(null,null);
        }

        private void refreshDataGrid(bool isRefreshPager,bool isSwitchFirstPage)
        {
            List<OrderInfo> list = new List<OrderInfo>();
            for (int i = Global.orderSuccessList.Count - 1; i > -1; i--)
            {
                string phoneNum = Global.orderSuccessList[i].m_OrderNum;
                string info = Global.orderSuccessList[i].ToString2();
                string time = Global.orderSuccessList[i].m_Time.ToString("yyyy-MM-dd HH:mm");
                string method = Global.orderSuccessList[i].m_OrderMethod;

                

                if(m_Filter != "")
                {
                    if (phoneNum.IndexOf(m_Filter) < 0 && info.IndexOf(m_Filter) < 0
                        && time.IndexOf(m_Filter) < 0 && method.IndexOf(m_Filter) < 0)
                        continue;
                }

                list.Add(Global.orderSuccessList[i]);
            }

            if (isRefreshPager)
            {
                this.pagerCtrl1.SetRecordCount(list.Count, isSwitchFirstPage);
                return;
            }

            this.dataGridView1.Rows.Clear();
            DataGridViewRow row;
            int firstNum = this.pagerCtrl1.PageSzie * (this.pagerCtrl1.CurrPageIndex - 1) + 1;
            int lastNum = this.pagerCtrl1.PageSzie * (this.pagerCtrl1.CurrPageIndex);
            for (int i=1; i <= list.Count; i++)
            {
                if (i >= firstNum && i <= lastNum)
                {
                    OrderInfo o = list[i - 1];
                    string info = o.ToString2();
                    string time = o.m_Time.ToString("yyyy-MM-dd HH:mm");
                    string method = o.m_OrderMethod;

                    this.dataGridView1.Rows.Add();
                    row = this.dataGridView1.Rows[this.dataGridView1.RowCount - 1];
                    row.Cells[0].Value = time;
                    row.Cells[1].Value = method;
                    row.Cells[2].Value = info;
                    row.Tag = o;
                }
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==13)
            {
                this.button1_Click(null, null);
            }
        }

        private void pagerCtrl1_OnRefreshClick(object sender, EventArgs e)
        {
            refreshDataGrid(false,false);
        }
    }
}