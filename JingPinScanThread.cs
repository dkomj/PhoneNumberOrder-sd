﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace PhoneNumberOrder
{
    public class JingPinScanThread
    {
        ManualResetEvent mSet;
        public JingPinScanThread(ManualResetEvent set)
        {
            mSet = set;
        }

        public void run()
        {
            MainFrm.self.scanJingPin(mSet);
        }
    }
}
