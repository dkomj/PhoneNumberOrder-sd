using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace PhoneNumberOrder
{
    public partial class SetNeedLockPhoneNumFrm : Form
    {
        public SetNeedLockPhoneNumFrm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                List<string[]> userList = Global.userList;
                if (userList.Count > 0 && MessageBox.Show("是否清空旧的预约帐号？", "确认", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    userList = new List<string[]>();


                Stream ms = null;
                try
                {
                    ms = new FileStream(openFileDialog1.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                    HSSFWorkbook workbook = new HSSFWorkbook(ms);
                    ISheet sheet = workbook.GetSheetAt(0);
                    IRow headerRow = sheet.GetRow(0);
                    int cellCount = headerRow.LastCellNum;
                    DataTable table = new DataTable();
                    for (int i = headerRow.FirstCellNum; i < cellCount; i++)
                    {
                        DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue);
                        table.Columns.Add(column);
                    }

                    int rowCount = sheet.LastRowNum;
                    for (int i = sheet.FirstRowNum + 1; i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);
                        DataRow dataRow = table.NewRow();

                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                                dataRow[j] = row.GetCell(j).ToString();
                        }
                        table.Rows.Add(dataRow);
                    }

                    lock (Global.needLockNumList)
                    {
                        foreach (DataRow row in table.Rows)
                        {


                            string strRule = row.IsNull("号码规则") ? "" : row["号码规则"].ToString();
                            string strPhoneNum = row.IsNull("手机号码") ? "" : row["手机号码"].ToString();

                            bool isContain = false;
                            foreach (string[] item in Global.needLockNumList)
                            {
                                if (item[1] == strPhoneNum)
                                {
                                    isContain = true;
                                    break;
                                }
                            }

                            if (!isContain)
                                Global.needLockNumList.Add(new string[] { strRule, strPhoneNum, "0"});
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("导入号码出错：" + ex.Message);
                    return;
                }
                finally
                {
                    if (ms != null)
                        ms.Close();
                }


                SetNeedLockPhoneNumFrm_Load(null, null);
                MessageBox.Show("导入成功！");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddNeedLockNum frm = new AddNeedLockNum();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                SetNeedLockPhoneNumFrm_Load(null, null);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否清空旧的预约帐号？", "确认", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                lock (Global.needLockNumList)
                {
                    Global.needLockNumList = new List<string[]>();
                    SetNeedLockPhoneNumFrm_Load(null, null);
                }
            }
        }

        private void SetNeedLockPhoneNumFrm_Load(object sender, EventArgs e)
        {
            this.dataGridView1.Rows.Clear();
            lock (Global.needLockNumList)
            {
                DataGridViewRow row;
                foreach (string[] item in Global.needLockNumList)
                {
                    this.dataGridView1.Rows.Add();

                    row = this.dataGridView1.Rows[this.dataGridView1.RowCount - 1];
                    row.Cells[0].Value = item[0];
                    row.Cells[1].Value = item[1];
                    row.Cells[2].Value = item[2];
                    row.Tag = item;
                }
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                string[] data = (string[])row.Tag;
                if (data[2] == "0")
                    data[2] = "1";
                else
                    data[2] = "0";
                row.Cells[2].Value = data[2];
            }
        }
    }
}